#!/usr/bin/python3

from bugzilla import Bugzilla
from jira import JIRA

# Connect to Bugzilla
bz = Bugzilla("bugzilla.redhat.com")
if not bz.logged_in:
    sys.exit("Not logged into Bugzilla.\n"
             "To set up:\n"
             " Log into Bugzilla web UI. Go to Preferences -> API Keys, generate an API key.\n"
             " Use the API key with: bugzilla login --api-key")

# Connect to Jira
with open("/home/michich/cnb-jira.token", "r") as token_file:
    token = token_file.read().strip()

jira = JIRA(
    options = {'server': "https://issues.redhat.com"},
    token_auth = token
)

# Obtain a mapping of custom Jira field names to internal IDs
fname2id = { f['name']:f['id'] for f in jira.fields() }
# For now, the only interesting field is:
bzbug_field_id = fname2id['Bugzilla Bug']

# Query Jira for RHELPLAN issues that are still open even though
# their BZ was migrated to the RHEL project.
my_issues = jira.search_issues(
    'project = "RHEL Planning" AND '\
    '"Pool Team" in (sst_network_drivers,sst_nic_rdma) AND '\
    'statusCategory != Done')

for issue in my_issues:
    # Get the Bugzilla Bug number
    try:
        bzdict = getattr(issue.fields, bzbug_field_id).__dict__
    except:
        print(f'{issue.key} has no Bugzilla Bug, skipping.')
        continue
    assert bzdict['siteid'] == 1
    bznum = bzdict['bugid']

    # Query the BZ info & check sanity
    bug = bz.getbug(bznum)
    if bug.status != 'CLOSED' or bug.resolution != 'MIGRATED':
        print(f'{issue.key};{bznum} is not CLOSED MIGRATED, skipping.')
        continue

    # Find the migration link
    migjira = None
    for b in bug.external_bugs:
        if b['is_migration_link'] == 1:
            # There can be only one!
            assert migjira is None
            assert b['type']['type'] == 'JIRA'
            assert b['type']['url'] == 'https://issues.redhat.com/'
            migjira = b['ext_bz_bug_id']
    assert migjira is not None
    print('{};{};{};{}'.format(issue.key, bznum, migjira, issue.fields.summary))

    # Find which transition is for moving to the Closed state
    tname2id = { t['name']:t['id'] for t in jira.transitions(issue) }
    closed_trans_id = tname2id['Closed']

    # Create a "duplicates" link and comment
    jira.create_issue_link(
        type='duplicates', inwardIssue=issue.key, outwardIssue=migjira,
        comment={'body':'The source Bugzilla bug ({}) this RHELPLAN issue is mirrored from was migrated to Jira: {}'.format(bznum, migjira)})

    # Close the issue
    jira.transition_issue(issue, closed_trans_id, resolution={'name':'Duplicate'})
