#!/usr/bin/python3

from bugzilla import Bugzilla
from jira import JIRA

# Connect to Bugzilla
bz = Bugzilla("bugzilla.redhat.com")
if not bz.logged_in:
    sys.exit("Not logged into Bugzilla.\n"
             "To set up:\n"
             " Log into Bugzilla web UI. Go to Preferences -> API Keys, generate an API key.\n"
             " Use the API key with: bugzilla login --api-key")

# Connect to Jira
with open("/home/michich/cnb-jira.token", "r") as token_file:
    token = token_file.read().strip()

jira = JIRA(
    options = {'server': "https://issues.redhat.com"},
    token_auth = token
)

# Obtain a mapping of custom Jira field names to internal IDs
fname2id = { f['name']:f['id'] for f in jira.fields() }
# For now, the only interesting field is:
bzbug_field_id = fname2id['Bugzilla Bug']

# Query Jira for RHELPLAN issues that are still open even though
# their BZ was migrated to the RHEL project.
my_issues = jira.search_issues(
    'project = "RHEL Planning" AND '\
    '"Pool Team" in (sst_network_drivers,sst_nic_rdma) AND '\
    'statusCategory != Done')

def linkify(prefix, value, align):
    if value is None:
        return 'None'.rjust(align)
    esc_url = '\x1b]8;;'
    esc_st  = '\x1b\\'
    return f'{esc_url}{prefix}{value}{esc_st}{str(value).rjust(align)}{esc_url}{esc_st}'

def jira_link(jid):
    return linkify('https://issues.redhat.com/browse/', jid, 14)

def bz_link(bz):
    return linkify('https://bugzilla.redhat.com/', bz, 8)

def emit(jid, bznum, jstate, bzstate, bzresol, migjira, summary):
    if bzstate == "CLOSED":
        st_res = f"{bzstate}:{bzresol}"
    else:
        st_res = bzstate

    print(f"{jira_link(jid)} {bz_link(bznum)} {jira_link(migjira)} {str(jstate):>8} {st_res:>16} {summary}")

for issue in my_issues:
    # Get the Bugzilla Bug number
    try:
        bzdict = getattr(issue.fields, bzbug_field_id).__dict__
    except:
        print(f'{jira_link(issue.key)} has no Bugzilla Bug, skipping.')
        continue
    assert bzdict['siteid'] == 1
    bznum = bzdict['bugid']
    bug = bz.getbug(bznum)

    # Find the migration link
    migjira = None
    for b in bug.external_bugs:
        if b['is_migration_link'] == 1:
            # There can be only one!
            assert migjira is None
            assert b['type']['type'] == 'JIRA'
            assert b['type']['url'] == 'https://issues.redhat.com/'
            migjira = b['ext_bz_bug_id']
    emit(issue.key, bznum, issue.fields.status, bug.status, bug.resolution, migjira, issue.fields.summary)

    if bug.status != "CLOSED":
        continue

    # If the original BZ is closed for whatever reason, close the RHELPLAN issue too

    # Find which transition is for moving to the Closed state
    tname2id = { t['name']:t['id'] for t in jira.transitions(issue) }
    closed_trans_id = tname2id['Closed']

    # Close the issue
    bzres2jira = {
            'ERRATA':'Done',
            'DUPLICATE':'Duplicate',
            'NOTABUG':'Not a Bug',
            'NEXTRELEASE':'Fixed',
            'CURRENTRELEASE':'Fixed',
            'WONTFIX':"Won't Do",
    }
    jira.transition_issue(issue, closed_trans_id, resolution={'name':bzres2jira[bug.resolution]}, comment=f'The source Bugzilla bug ({bznum}) this RHELPLAN issue is mirrored from is CLOSED:{bug.resolution}.')

#    # Create a "duplicates" link and comment
#    jira.create_issue_link(
#        type='duplicates', inwardIssue=issue.key, outwardIssue=migjira,
#        comment={'body':'The source Bugzilla bug ({}) this RHELPLAN issue is mirrored from was migrated to Jira: {}'.format(bznum, migjira)})
#
