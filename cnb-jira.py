#!/usr/bin/python3

from jira import JIRA

with open("/home/michich/cnb-jira.token", "r") as token_file:
    token = token_file.read().strip()

jira = JIRA(
    options = {'server': "https://issues.redhat.com"},
    token_auth=token
)

my_issues = jira.search_issues('project=RHEL and assignee=currentUser()')

for issue in my_issues:
    print('{}: {}'.format(issue.key, issue.fields.summary))
