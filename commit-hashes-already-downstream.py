#!/usr/bin/python3
# Looks for Jiras with Commit Hashes that already have a downstream commit per kerneloscope
# or that are already included in an open merge request.

from jira import JIRA
from gitlab import Gitlab
import re, subprocess, sys

# RHEL parameters
FIX_VERSION       = 'rhel-9.6'
KERNELOSCOPE_TREE = 'rhel-9.6'
GITLAB_WEB        = 'https://gitlab.com'
GITLAB_PROJECT    = 'redhat/centos-stream/src/kernel/centos-stream-9'
GIT_BRANCH        = 'main'
JIRA_WEB          = 'https://issues.redhat.com'
# Local user configuration
GIT_DIR           = '/home/michich/git/kernel'
GIT_REMOTE        = 'centos-stream-9'
GITLAB_TOKEN_FILE = '/home/michich/gitlab-cnb-queries.token'
JIRA_TOKEN_FILE   = '/home/michich/cnb-jira.token'
POOL_TEAM         = 'rhel-sst-network-drivers'

print('Querying kerneloscope ...')
ks = subprocess.run(['kerneloscope', 'log', '-i', '-n', '1', '--tree', KERNELOSCOPE_TREE], capture_output=True)
if ks.returncode != 0 or ks.stderr != b'':
    print('Kerneloscope returned an error:')
    print(ks.stderr)
    sys.exit(1)
ks_top_commit = ks.stdout.decode('ascii').rstrip()
assert re.fullmatch(r'[a-f0-9]{40}', ks_top_commit)

print(f'kerneloscope top commit in "{KERNELOSCOPE_TREE}" is {ks_top_commit}')

print(f'Listing open MRs in gitlab {GITLAB_WEB}/{GITLAB_PROJECT} ...')
with open(GITLAB_TOKEN_FILE, "r") as token_file:
    gl_token = token_file.read().strip()

gl = Gitlab(GITLAB_WEB, private_token=gl_token)
#gl.enable_debug()
#gl.auth()

gl_project = gl.projects.get(GITLAB_PROJECT)

gl_MRs = gl_project.mergerequests.list(state='opened', get_all=True)
gl_MRs_by_id = {}
for gl_mr in gl_MRs:
   gl_MRs_by_id[gl_mr.iid] = gl_mr
print(list(gl_MRs_by_id.keys()))

print(f'Fetching from {GIT_REMOTE} ...')
subprocess.run(['git', '-C', GIT_DIR, 'fetch', GIT_REMOTE])

def find_mentioned_commits(mentions, rev_from, rev_to, mr):
    current_commit = None
    gitlog = subprocess.run(['git', '-C', GIT_DIR, 'log', '--no-decorate', f'{rev_from}..{rev_to}'], capture_output=True)
    if gitlog.returncode != 0 or gitlog.stderr != b'':
        print('git log returned an error')
        print(gitlog.stderr)
        sys.exit(1)
    for line in gitlog.stdout.splitlines():
        m = re.match(b'(.*)commit ([0-9a-f]{40})', line)
        if m:
            commit_id = m.group(2).decode('ascii')
            if m.group(1) == b'':
                current_commit = commit_id
            else:
                mentions.setdefault(commit_id, []).append({'MR':mr, 'id':current_commit})

def make_clickable_link(url, text):
    esc_url = "\x1b]8;;"
    esc_st  = "\x1b\\"
    #esc_bold =   "\x1b[1m"
    #esc_normal = "\x1b[0m"
    return f"{esc_url}{url}{esc_st}{text}{esc_url}{esc_st}"

def make_clickable_MR_link(mr, with_title):
    link = make_clickable_link(f'{GITLAB_WEB}/{GITLAB_PROJECT}/-/merge_requests/{mr.iid}', f'!{mr.iid}')
    if with_title:
        return f'{link} {mr.title}'
    else:
        return link

print('Scanning recent commits that kerneloscope might not yet know about ...')
mentions = {}
find_mentioned_commits(mentions, f'{ks_top_commit}', f'{GIT_REMOTE}/{GIT_BRANCH}', 'already merged')

print('Scanning commits in MRs: ')
for mr in gl_MRs_by_id.values():
    print(make_clickable_MR_link(mr, False), end=' ', flush=True)
    find_mentioned_commits(mentions, f'{GIT_REMOTE}/{GIT_BRANCH}', f'{GIT_REMOTE}/merge-requests/{mr.iid}', mr.iid)
print()

print('Querying Jira ...')
with open(JIRA_TOKEN_FILE, "r") as token_file:
    jira_token = token_file.read().strip()

jira = JIRA(
    options = {'server': JIRA_WEB },
    token_auth=jira_token
)

# Obtain a mapping of custom Jira field names to internal IDs
fname2id = { f['name']:f['id'] for f in jira.fields() }
# For now, the only interesting field is:
commit_hashes_field_id = fname2id['Commit Hashes']

my_issues = jira.search_issues(
        f'project=RHEL and status in (New, Planning, "In Progress") and "Commit Hashes" is not EMPTY and "Pool Team" = {POOL_TEAM} and fixVersion = {FIX_VERSION}',
        maxResults = 1000
        )

def print_jira_title(issue):
    url = f"{JIRA_WEB}/browse/{issue.key}"
    print(f'• {make_clickable_link(url, url)} {issue.fields.summary}')

def print_jira_title_once(issue, printed_title):
    if not printed_title[0]:
        print_jira_title(issue)
        printed_title[0] = True

def short_to_full_commit_id(cid):
    revparse = subprocess.run(['git', '-C', GIT_DIR, 'rev-parse', cid], capture_output=True)
    if revparse.returncode !=0 or revparse.stderr != b'':
        return None

    full_id = revparse.stdout.decode('ascii').rstrip()
    # Verify that cid was a (short) commit id and not another kind of a tree-ish
    return full_id if full_id.startswith(cid) else None

def check_kerneloscope(issue, commit, printed_title):
    ks = subprocess.run(['kerneloscope', 'downstream', '-i', '-t', KERNELOSCOPE_TREE, ch], capture_output=True)
    if ks.returncode != 0 or ks.stderr != b'':
        print_jira_title_once(issue, printed_title)
        print(f"Kerneloscope returned an error:")
        print(ks.stderr)
        return False

    if ks.stdout != b'':
        print_jira_title_once(issue, printed_title)
        for downstream in ks.stdout.decode('ascii').splitlines():
            assert re.fullmatch(r'[0-9a-f]{40}', downstream)
            print(f'  {ch} -> {downstream}')
            subprocess.run(f'git -C {GIT_DIR} show -s {downstream} | grep JIRA:', shell=True)
        return True

    return False

def check_mrs(issue, commit, printed_title):
    commit_mentions = mentions.get(commit)
    if not commit_mentions:
        return False

    #print_jira_title_once(issue, printed_title)
    for ment in commit_mentions:
        mr_id = ment['MR']
        gl_mr = None
        mr_desc = ''
        if mr_id != 'already merged':
            gl_mr = gl_MRs_by_id[mr_id]
            mr_desc = gl_mr.description
        mr_refs_this_jira = False
        for desc_line in mr_desc.splitlines():
            m = re.match(r'JIRA: (\S+)', desc_line)
            if m and m.group(1) == f'{JIRA_WEB}/browse/{issue.key}':
                mr_refs_this_jira = True
        if mr_id != 'already merged':
            msg = f'  {commit} is referenced from {ment["id"]} in {make_clickable_MR_link(gl_mr, True)}'
        else:
            msg = f'  {commit} is referenced from {ment["id"]}, already merged'

        if mr_refs_this_jira:
            #print_jira_title_once(issue, printed_title)
            #print(f'{msg}, which references this Jira')
            pass
        else:
            print_jira_title_once(issue, printed_title)
            print(msg)

    return True

for issue in my_issues:
    commit_hashes_str = getattr(issue.fields, commit_hashes_field_id)
    commit_hashes = commit_hashes_str.splitlines()
    printed_title = [ False ]
    for ch in commit_hashes:
        ch = ch.split()[0]
        full_id = short_to_full_commit_id(ch)
        if not full_id:
            print(f'{issue.key}: Not a valid commit id: {ch}')
            continue
        if check_kerneloscope(issue, full_id, printed_title):
            continue
        check_mrs(issue, full_id, printed_title)
