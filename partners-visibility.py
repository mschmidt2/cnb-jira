#!/usr/bin/python3
# Make sure partner engineers can see the Jira issues they are Assignees of.

# Mapping of partner engineer accounts to Contributing Groups names
partners = {
        "sreeredd": "Broadcom DCSG Confidential Group",
        "rh-ee-bpoirier": "NVIDIA (Mellanox) Confidential Group",
}

from jira import JIRA

with open("/home/michich/cnb-jira.token", "r") as token_file:
    token = token_file.read().strip()

jira = JIRA(
    options = {'server': "https://issues.redhat.com"},
    token_auth=token
)

# Obtain a mapping of custom Jira field names to internal IDs
fname2id = { f['name']:f['id'] for f in jira.fields() }
contrib_groups_fid = fname2id['Contributing Groups']
security_level_fid = fname2id['Security Level']

for (name, group) in partners.items():
    print(f'Assignee: {name}')
    print("Adding the partner's Contributing Group where it's missing...")
    issues = jira.search_issues(
            f'project=RHEL and statusCategory!=Done and assignee="{name}" and level is not EMPTY and ("Contributing Groups" is EMPTY or "Contributing Groups"!="{group}")')
    for issue in issues:
        print(f'{issue.key}: {issue.fields.summary}')
        issue.add_field_value(contrib_groups_fid, {'name':group})

    print("Adjusting Security Level where it's too restrictive...")
    issues = jira.search_issues(
            f'project=RHEL and statusCategory!=Done and assignee="{name}" and "Contributing Groups"="{group}" and level="Red Hat Engineering Authorized"')
    for issue in issues:
        print(f'{issue.key}: {issue.fields.summary}')
        issue.update(fields={security_level_fid:{"name":"Red Hat Partner"}})
